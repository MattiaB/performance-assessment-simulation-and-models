## Introduction
This is a project for the performance assessment-simulation and models exam,
allow to analyze a system with a queue network that connect the nodes
and with nodes where the client pass through.
is structured in three parts:

1. Operation Analysis allow to analyze a system with all nodes that have a exponential
service time
2. Markov chain Analysis allow to analyze a system with some node with
a service time distribution such Erlang-k or Iper-Exponential
3. The Simulator allow to simulate a system
and node can have any distribution service time and any queue

## Requirements

* Python 2.7
* [NumPy](http://www.numpy.org/, "NumPy library")
* [SciPy](http://www.scipy.org/, "SciPy library") (only for the simulator)

## Run

Before run a file (operational\_analysis.py, markov\_chain.py, simulator.py)
you must set the variable PYTHONPATH with the root of the project folder

    :::bash
        export PYTHONPATH='root project directory'

The running parameters are:

1. The path of the input data file (Required)
2. The path of the json output data file (Required)
3. If you want also the csv file with almost all output data put "csv"

The csv file will be created with the same name of json output data but with final string ".csv"

## Hosted

Operation and Markov analysis [http://performance-analysis.herokuapp.com/](http://performance-analysis.herokuapp.com/, "Performance analysis Software")
