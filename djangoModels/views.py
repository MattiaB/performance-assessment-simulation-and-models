# Create your views here.
import json

from django.shortcuts import render, redirect
from django import forms
from djangoSettings.settings import STATICFILES_DIRS

from models import Model
from operationAnalysis.operational_analysis import operation_analysis
from markovChianAnalysis.markov_chain import markov_chain_analysis

class ModelForm(forms.Form):
    title = forms.CharField(max_length=200)
    json_input = forms.CharField(widget=forms.Textarea)


def index(request):
    all_models = Model.objects.all()
    context = {'all_models_list': all_models}
    return render(request, 'all_models.html', context)


def add_model(request):
    if request.method == 'POST': # If the form has been submitted...
        if not request.POST.get('onlyJsonData'):
            data = {'Q': {}, 'nodes': []}
            q = zip(request.POST.getlist('from'), request.POST.getlist('to'), request.POST.getlist('value'))
            for line in q:
                data['Q'][line[0]] = {}
                data['Q'][line[0]][line[1]] = line[2]

            nodes = zip(request.POST.getlist('id'), request.POST.getlist('serviceTime'),
                        request.POST.getlist('name'), request.POST.getlist('type'),
                        request.POST.getlist('otherParams'))
            for node in nodes:
                node_dict = {
                    'id':   int(node[0]),
                    'S':    node[1],
                    'name': node[2],
                    'type': node[3]
                }
                other_params = node[4].split('=')
                for other_param_index in range(0, len(other_params), 2):
                    node_dict[other_params[other_param_index]] = other_params[other_param_index + 1]
                data['nodes'].append(node_dict)

            data['reference_node'] = int(request.POST.get('referenceNode'))
            if request.POST.get('valueForEachNumberOfClient'):
                data['all_n'] = True
            else:
                data['all_n'] = False
            data['N'] = int(request.POST.get('numberOfClients'))

            json_input = json.dumps(data, sort_keys=True,
                                         indent=4, separators=(',', ': '))
        else:
            json_input = request.POST.get('json_input')
        model = Model(title=request.POST.get('title'), json_input=json_input)
        model.save()
        return redirect('view_model', str(model.id)) # Redirect after POST
    else:
        form = ModelForm() # An unbound form

    return render(request, 'add_model.html', {
        'form': form,
    })


def modify_model(request, id):
    model = Model.objects.get(id=id)
    if request.method == 'POST': # If the form has been submitted...
        form = ModelForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            model.title = form.cleaned_data['title']
            model.json_input = form.cleaned_data['json_input']
            model.save()
            return redirect('view_model', str(model.id)) # Redirect after POST
    else:
        form = ModelForm() # An unbound form

    return render(request, 'modify_model.html', {
        'form': form,
        'model': model,
    })


def view_model(request, id):
    model = Model.objects.get(id=id)
    if model.markov_analysis_json_output:
        markov_analysis_json_output = json.loads(model.markov_analysis_json_output)
        model.markov_analysis_json_output = json.dumps(markov_analysis_json_output)
        model.markov_analysis_pretty_json_output = json.dumps(markov_analysis_json_output, indent=4)

    if model.operation_analysis_json_output:
        operation_analysis_json_output = json.loads(model.operation_analysis_json_output)
        model.operation_analysis_json_output = json.dumps(operation_analysis_json_output)
        model.operation_analysis_pretty_json_output = json.dumps(operation_analysis_json_output, indent=4)

    return render(request, 'view_model.html', {'model': model})


def delete_model(request, request_id):
    model = Model.objects.get(id=request_id)
    model.delete()
    return redirect('index')


def simulate_model(request, request_id):
    model = Model.objects.get(id=request_id)
    input_data = json.loads(model.json_input)
    input_data['plot_file_path'] = STATICFILES_DIRS[0] + '/' + model.chart_path()
    output_data, output_data_csv = operation_analysis(input_data)
    model.set_json_output(output_data)
    model.save()
    return redirect('view_model', model.id)


def operation_analysis_model(request, request_id):
    model = Model.objects.get(id=request_id)
    input_data = json.loads(model.json_input)
    output_data, output_data_csv = operation_analysis(input_data)
    model.set_operation_analysis_json_output(output_data)
    model.save()
    return redirect('view_model', model.id)


def markov_analysis_model(request, request_id):
    model = Model.objects.get(id=request_id)
    input_data = json.loads(model.json_input)
    output_data, output_data_csv = markov_chain_analysis(input_data)
    model.set_markov_analysis_json_output(output_data)
    model.save()
    return redirect('view_model', model.id)