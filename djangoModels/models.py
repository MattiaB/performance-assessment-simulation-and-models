import json

from django.db import models

# Create your models here.


class Model(models.Model):
    title = models.CharField(max_length=200)
    json_input = models.TextField()
    operation_analysis_json_output = models.TextField()
    markov_analysis_json_output = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        input_data = json.loads(self.json_input)
        self.json_input = json.dumps(input_data, sort_keys=True,
                                     indent=4, separators=(',', ': '))
        super(Model, self).save(*args, **kwargs)

    def set_operation_analysis_json_output(self, output_data):
        self.operation_analysis_json_output = json.dumps(output_data, sort_keys=True,
                                      indent=4, separators=(',', ': '))

    def set_markov_analysis_json_output(self, output_data):
        self.markov_analysis_json_output = json.dumps(output_data, sort_keys=True,
                                      indent=4, separators=(',', ': '))

    def __unicode__(self):
        return self.title