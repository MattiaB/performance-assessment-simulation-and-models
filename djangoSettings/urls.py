from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from djangoModels import views

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'djangoSettings.views.home', name='home'),
                       # url(r'^djangoSettings/', include('djangoSettings.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),

                       url(r'^$', views.index, name='index'),
                       url(r'^model/add', views.add_model, name='add_model'),
                       url(r'^model/(\d{1,4})/modify', views.modify_model, name='modify_model'),
                       url(r'^model/(\d{1,4})/delete', views.delete_model, name='delete_model'),
                       url(r'^model/(\d{1,4})/operation_analysis', views.operation_analysis_model, name='operation_analysis_model'),
                       url(r'^model/(\d{1,4})/markov_analysis', views.markov_analysis_model, name='markov_analysis_model'),
                       url(r'^model/(\d{1,4})', views.view_model, name='view_model'),


)
