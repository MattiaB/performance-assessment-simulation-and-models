import json
import csv
import numpy as np

__author__ = 'Mattia Bertorello'

NODES = 'nodes'
NUMBER_OF_CUSTOMERS = 'N'
REFERENCE_NODE = 'reference_node'


class GeneralConfigurations(object):
    def convert_input_data(self, input_data):
        """
        Convert dict input data into object as own member variable
        :type input_data: dict
        :param input_data:
        """
        pass

    def convert_output_data(self, output_data):
        """
        Convert the object that cannot be convert with normal to string for json conversion
        :type output_data: dict
        :param output_data:
        :return:
        """
        pass

    def convert_output_data_for_csv(self, output):
        """
        Convert the output data into a list of list for a simple conversion into csv format
        :type output: dict
        :param output:
        :return: list of list
        """
        pass


def get_queue_network_from_input_data(input_data):
    """
    Convert hash representation into matrix representation of queue network

    :type input_data: dict
    :param input_data:
    :return:numpy matrix
    """
    num_nodes = len(input_data[NODES])
    # Conversione dei dati forniti a dizionario in una matrice numpy
    queue_network = np.zeros(shape=(num_nodes, num_nodes), dtype=getFloatNumpyType())
    for key_row, value_line in input_data['Q'].iteritems():
        for key_column, value in value_line.iteritems():
            queue_network[int(key_row), int(key_column)] = toFloatNumpy(value)

    return queue_network


def get_nodes_from_input_data(input_data, convert_node_function):
    """
    Generate a list of node object from a dict representation of nodes


    :type input_data: dict
    :type convert_node_function: function
    :param input_data:
    :param convert_node_function: a function that takes
            these parameters node_dict, node_type, node_id, node_name, service_time
    :return: list of node objects
    """
    nodes = [None] * len(input_data[NODES])
    for node_dict in input_data.get(NODES, []):
        node_type = node_dict['type']
        node_id = int(node_dict['id'])
        node_name = node_dict.get('name', str(node_id))
        service_time = toFloatNumpy(node_dict.get('S', 0))
        node = convert_node_function(node_dict, node_type, node_id, node_name, service_time)
        nodes[node_id] = node

    return nodes


def get_convert_standard_node_function(single_server_class, infinite_server_class):
    def convert_node(node_dict, node_type, node_id, node_name, service_time):
        node = None
        if node_type == 'single_server':
            node = single_server_class(node_id, node_name, service_time)
        elif node_type == 'infinite_server':
            node = infinite_server_class(node_id, node_name, service_time)
        return node

    return convert_node


def convert_output_data(output, number_of_customers):
    convert_output = dict()
    convert_output[NODES] = map(convert_node, output[NODES])
    convert_output[NUMBER_OF_CUSTOMERS] = number_of_customers
    return convert_output


def convert_node(node):
    """




    :type node: Node
    :param node:
    """
    out = dict()
    out['id'] = node.id
    out['name'] = node.name
    out['response_time_N'] = fromNumpyTypeToString(node.response_time_N)
    out['throughput_N'] = fromNumpyTypeToString(node.throughput_N)
    out['load_N'] = fromNumpyTypeToString(node.load_N)
    return out


def get_input_data_from_file_json(file_name):
    json_file = open(file_name, 'r')
    input_json = json.loads(json_file.read())
    json_file.close()
    return input_json


def write_output_data(output_dict, output_filename):
    output_json = json.dumps(output_dict, sort_keys=True,
                             indent=4, separators=(',', ': '))
    f = open(output_filename, mode='w')
    f.write(output_json)
    f.close()


def write_output_data_csv(rows, output_filename):
    with open(output_filename + '.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(rows)


def getFloatNumpyType():
    return np.float64


def toFloatNumpy(value):
    return np.float64(value)


def fromNumpyTypeToString(numpy_object_type):
    import types

    if isinstance(numpy_object_type, types.ListType):
        return [item.astype('|S20') for item in numpy_object_type]
    else:
        return numpy_object_type.astype('|S20')