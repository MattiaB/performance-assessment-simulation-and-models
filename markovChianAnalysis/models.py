__author__ = 'Mattia Bertorello'


class Node(object):
    def __init__(self, id_param, name, service_time_value=None):
        self.id = id_param
        self.name = name
        self.service_time_value = service_time_value
        self.visit_ratio = 0
        self.load_dependent = False
        self.max_states = 1

    def service_time(self, queue_len=1):
        return self.service_time_value

    def state_service_time(self, state):
        return self.service_time(state[self.id][0])

    def get_empty_tuple(self):
        return 0, 0

    def get_tuple_first_state(self, n):
        return n, 1

    def is_max_state(self, state):
        if state:
            return state[self.id][1] == self.max_states
        return False

    def next_internal_state(self, state):
        state = list(state)
        state[self.id] = list(state[self.id])
        state[self.id][1] += 1
        state[self.id] = tuple(state[self.id])
        return tuple(state)

    def add_client(self, node_tuple, value_to_add):
        node_tuple = list(node_tuple)
        node_tuple[0] += value_to_add
        if node_tuple[0]:
            if node_tuple[1] > self.max_states or not node_tuple[1]:
                return node_tuple[0], 1
            else:
                return tuple(node_tuple)
        else:
            return 0, 0


class SingleServer(Node):
    pass


class InfiniteServer(Node):
    def __init__(self, id_param, name, service_time_value=None):
        super(InfiniteServer, self).__init__(id_param, name, service_time_value)
        self.load_dependent = True

    def service_time(self, queue_len=1):
        return super(InfiniteServer, self).service_time() / queue_len

    def state_service_time(self, state):
        return self.service_time(state[self.id][0])


class ErlangServer(Node):
    def __init__(self, id_param, name, service_time_value=None, erlang_k=1, erlang_node_number=0, number_of_nodes=1):
        super(ErlangServer, self).__init__(id_param, name, service_time_value)
        self.max_states = erlang_k

    def state_service_time(self, state):
        return super(ErlangServer, self).service_time() / self.max_states


class States:
    def __init__(self, nodes):
        self.nodes = nodes
        self.state_number = 0
        self.dict = {}
        self.transitions_dict = {}
        self.list = []

    def add_transition(self, q, from_node, from_state, to_state):
        number_from_state = self.dict[from_state]
        to_state_number = self.dict.get(to_state, self.state_number)
        transition = (number_from_state, to_state_number)
        mu = 1 / from_node.state_service_time(from_state)
        self.transitions_dict[transition] = mu * q

    def add_state(self, new_state):
        if not new_state in self.dict:
            self.list.append(new_state)
            self.dict[new_state] = self.state_number
            self.state_number += 1