from helpers.convert_data import *
from markovChianAnalysis.models import ErlangServer, SingleServer, InfiniteServer

__author__ = 'Mattia Bertorello'

PI = 'pi'
TRANSITIONS = 'transitions'
STATES = 'states'
NUM_STATES = 'number_of_states'
NUM_TRANSITIONS = 'number_of_transitions'
ALL_N = 'all_n'
EXTRA_DATA = 'extra_data'


class MarkovChainConfigurations(GeneralConfigurations):
    def convert_input_data(self, input_data):
        self.queue_network = get_queue_network_from_input_data(input_data)

        input_data['erlang_node_number'] = 0
        number_of_nodes = len(input_data[NODES])
        standard_node_function = get_convert_standard_node_function(SingleServer, InfiniteServer)

        def convert_node_function(node_dict, node_type, node_id, node_name, service_time):
            node = standard_node_function(node_dict, node_type, node_id, node_name, service_time)
            if node_type == 'erlang_server':
                erlang_k = int(node_dict.get('erlang_k', 1))
                node = ErlangServer(node_id, node_name, service_time, erlang_k, input_data['erlang_node_number'],
                                    number_of_nodes)
                input_data['erlang_node_number'] += 1
            node.throughput_N = []
            node.response_time_N = []
            node.mean_number_of_customers_N = []
            node.load_N = []
            return node

        self.nodes = get_nodes_from_input_data(input_data, convert_node_function)
        self.reference_node = input_data.get(REFERENCE_NODE, 0)
        self.number_of_customers = input_data.get(NUMBER_OF_CUSTOMERS, 1)
        self.all_n = input_data.get(ALL_N, False)
        self.extra_data = input_data.get(EXTRA_DATA, False)

    def convert_output_data(self, output_data):
        converted_output_data = convert_output_data(output_data, self.number_of_customers)

        def convert_extra_data(pi, transitions_input, states_input):
            pi = list(fromNumpyTypeToString(pi))
            transitions = {}
            for transition, value in transitions_input.iteritems():
                transitions[str(transition)] = fromNumpyTypeToString(value)
            states = {}
            for state, value in states_input.iteritems():
                states[str(state)] = value
            return pi, transitions, states

        converted_output_data[EXTRA_DATA] = {}
        extra_data = {}
        for i in range(len(output_data[PI])):
            internal_extra_data = {}
            pi, transitions, states = convert_extra_data(output_data[PI][i], output_data[TRANSITIONS][i],
                                                         output_data[STATES][i])
            internal_extra_data[PI] = pi
            internal_extra_data[TRANSITIONS] = transitions
            internal_extra_data[STATES] = states
            internal_extra_data[NUM_STATES] = len(states)
            internal_extra_data[NUM_TRANSITIONS] = len(transitions)
            extra_data[str(i + 1)] = internal_extra_data

        converted_output_data[EXTRA_DATA] = extra_data

        return converted_output_data

    def convert_output_data_for_csv(self, output):
        nodes = output[NODES]
        columns = list()
        header = list()
        columns.append(range(1, output[NUMBER_OF_CUSTOMERS] + 1))
        header.append('N')
        for node in nodes:
            columns.append(node['throughput_N'])
            header.append('throughput_N_' + str(node['id']))

        for node in nodes:
            columns.append(node['response_time_N'])
            header.append('response_time_N_' + str(node['id']))

        for node in nodes:
            columns.append(node['load_N'])
            header.append('load_N' + str(node['id']))

        csv_output_data = zip(*columns)
        csv_output_data = [header] + csv_output_data
        return csv_output_data