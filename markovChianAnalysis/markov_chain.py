# coding=utf-8

import numpy as np
from helpers.convert_data import toFloatNumpy
from markovChianAnalysis.cofigurations import *
from models import *


__author__ = 'Mattia Bertorello'


def set_nodes_properties(pi_N, nodes, number_of_customers, states_dict):
    """
    Calculate the basic node properties got from steady state `pi_N`
    :type pi_N: numpy.ndarray
    :type nodes: list
    :type number_of_customers: int
    :type states_dict: dict
    :rtype : list of nodes
    :param pi_N:
    :param nodes:
    :param number_of_customers:
    :param states_dict:
    :return:
    """
    for node in nodes:
        throughput = toFloatNumpy(0)
        for n in range(1, number_of_customers + 1):
            filter_state = filter(lambda position: position[0][node.id][0] == n, states_dict.iteritems())
            marginals = sum(map(lambda position: pi_N[position[1]], filter_state))

            mu = 1 / node.service_time(n)
            throughput += mu * marginals

        response_time = number_of_customers / throughput - node.service_time_value
        load = throughput * node.service_time(number_of_customers)
        node.throughput_N += [throughput]
        node.response_time_N += [response_time]
        node.load_N += [load]
    return nodes


def markov_chain(config):
    import time

    queue_network = config.queue_network
    nodes = config.nodes
    number_of_customers = config.number_of_customers
    reference_node = config.reference_node
    if config.all_n:
        number_of_customers_range_analysis = range(1, number_of_customers + 1)
    else:
        number_of_customers_range_analysis = [number_of_customers]
    output_data = dict()
    output_data[PI] = []
    output_data[TRANSITIONS] = []
    output_data[STATES] = []
    for number_of_customers in number_of_customers_range_analysis:
        print 'Start analysis with {} clients'.format(number_of_customers)
        start = time.time()
        transitions_matrix, states_dict, transitions_dict = \
            generate_transition_probability_matrix(queue_network, nodes, number_of_customers, reference_node)
        pi_N = steady_state_distribution(transitions_matrix)
        nodes = set_nodes_properties(pi_N, nodes, number_of_customers, states_dict)
        if config.extra_data:
            output_data[PI].append(pi_N)
            output_data[TRANSITIONS].append(transitions_dict)
            output_data[STATES].append(states_dict)
        end = time.time()
        print 'End analysis with {} clients, time spent: {}'.format(number_of_customers, end - start)

    output_data[NODES] = nodes

    return output_data


def steady_state_distribution(transitions_matrix):
    """
    Create the steady state distribution from the transition matrix
    :type transitions_matrix: numpy.ndarray
    :rtype : numpy.ndarray
    :param transitions_matrix: 
    :return: 
    """
    reference_equation = 0
    transitions_matrix = transitions_matrix.transpose()
    N = len(transitions_matrix)
    for i in range(N):
        transitions_matrix[reference_equation][i] = toFloatNumpy(1.0)
    coefficients = np.zeros(N, dtype=getFloatNumpyType())
    coefficients[reference_equation] = toFloatNumpy(1.0)
    pi_N = np.linalg.solve(transitions_matrix, coefficients)
    return pi_N


def generate_transition_probability_matrix(queue_network, nodes, number_of_customers, reference_node):
    """
    Creates the transition matrix from a queue network and some nodes
    with a specific number of client into the system
    :type reference_node: int
    :type number_of_customers: int
    :type nodes: list
    :type queue_network: numpy.ndarray,
    :rtype : numpy.ndarray, numpy.ndarray, numpy.ndarray
    :param queue_network:
    :param nodes:
    :param number_of_customers:
    :param reference_node:
    :return:transitions matrix, states dict, transitions dict
    """
    number_of_nodes = len(nodes)

    # Creazione dello stato iniziale
    tuples = map(lambda n: n.get_empty_tuple(), nodes)
    tuples[reference_node] = nodes[reference_node].get_tuple_first_state(number_of_customers)
    first_tuple = tuple(tuples)

    # Generazione dei vettori differenza per ogni nodo
    for node in nodes:
        difference_vector = [0] * number_of_nodes
        node_id = node.id
        for i in range(0, number_of_nodes):
            if queue_network[node_id][i]:
                difference_vector[i] = 1
        node.difference_vector = tuple(difference_vector)

    states = States(nodes)
    states.add_state(first_tuple)
    states_list = states.list
    states_dict = states.dict

    # Algoritmo per generare gli stati e i valori delle transizioni
    while states_list:
        state = states_list.pop()
        # Ottengo i nodi che hanno dei clienti nello stato attuale
        for from_node in filter(lambda node: state[node.id][0], nodes):
            # Ottengo l'indice del vettore differenza dove quel valore è diverso da zero
            # e quindi c'è una transizione da fare
            for node_index, difference_value in filter(lambda v: v[1],
                                                       enumerate(from_node.difference_vector)):
                to_node = nodes[node_index]
                # Creazione degli stati interni al nodo
                internal_state = state
                while not from_node.is_max_state(internal_state):
                    next_state = from_node.next_internal_state(internal_state)
                    states.add_state(next_state)
                    states.add_transition(1, from_node, internal_state, next_state)
                    internal_state = next_state

                # Creazione dello stato successivo
                to_state = from_node.next_internal_state(internal_state)
                to_state = list(to_state)
                to_state[from_node.id] = from_node.add_client(to_state[from_node.id], -difference_value)
                to_state[to_node.id] = to_node.add_client(to_state[to_node.id], difference_value)
                to_state = tuple(to_state)

                states.add_state(to_state)
                states.add_transition(queue_network[from_node.id][to_node.id],
                                      from_node, internal_state, to_state)

    transitions_dict = states.transitions_dict
    #Creazione della matrice di delle probabilità di transizione
    num_states = len(states_dict)
    transitions_matrix = np.zeros(shape=(num_states, num_states), dtype=getFloatNumpyType())
    for position, probability in transitions_dict.iteritems():
        transitions_matrix[position] = toFloatNumpy(probability)
    for i, row in enumerate(transitions_matrix):
        filtered_row = map(lambda t: t[1], filter(lambda t: t[0] != i, enumerate(row)))
        transitions_matrix[i][i] = - sum(filtered_row)
    return transitions_matrix, states_dict, transitions_dict


def markov_chain_analysis(input_data, do_csv=False):
    config = MarkovChainConfigurations()
    config.convert_input_data(input_data)
    output_data = markov_chain(config)
    output_data = config.convert_output_data(output_data)
    output_data_csv = []
    if do_csv:
        output_data_csv = config.convert_output_data_for_csv(output_data)
    return output_data, output_data_csv


if __name__ == '__main__':
    import sys

    file_name = sys.argv[1]
    output_filename = sys.argv[2]
    do_csv = False
    if len(sys.argv) >= 4 and sys.argv[3] == 'csv':
        do_csv = True
    input_data = get_input_data_from_file_json(file_name)
    output_data, output_data_csv = markov_chain_analysis(input_data, do_csv)
    write_output_data(output_data, output_filename)
    if do_csv:
        write_output_data_csv(output_data_csv, output_filename)