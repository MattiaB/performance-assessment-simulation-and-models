from numpy import array

from helpers.convert_data import *
from simulationAnalysis.models import FIFOQueue, SingleServer, InfiniteServer, ErlangServer, ProjectCPU
from simulationAnalysis.stats import check_on_response_time

__author__ = 'Mattia Bertorelllo'

PRECISION_INTERVAL = 'precision'
NODES = 'nodes'
CONFIDENCE_INTERVAL = 'confidence_interval'
TEST_SIMULATOR = 'test'
SIMULATOR_SEED = 'seed'
SIMULATION_RUNS = 'runs'
THEORETICAL_VALUE = 'theoretical_value'
MEAN_INTERVAL = 'mean_interval'
MAX_INTERVAL = 'max_interval'
MIN_INTERVAL = 'min_interval'
KEEP_INTERVALS = 'keep_intervals'
STEPS_NUMBER = 'steps_number'
POINT_OF_REGENERATION_COUNTER = 'point_of_regeneration_counter'


class SimulatorConfigurations(GeneralConfigurations):
    def convert_input_data(self, input_data):
        self.input_data = input_data
        self.queue_network = get_queue_network_from_input_data(input_data)
        self.reference_node = input_data.get(REFERENCE_NODE, 0)
        self.number_of_customers = input_data.get(NUMBER_OF_CUSTOMERS, 1)
        self.test_simulator = input_data.get(TEST_SIMULATOR, False)
        self.simulator_seed = input_data.get(SIMULATOR_SEED, None)
        self.simulation_runs = input_data.get(SIMULATION_RUNS, 1)
        self.theoretical_value = toFloatNumpy(input_data.get(THEORETICAL_VALUE, 0))
        self.keep_intervals = input_data.get(KEEP_INTERVALS, False)
        self.precision = input_data.get(PRECISION_INTERVAL, 0.1)
        self.steps_number = input_data.get(STEPS_NUMBER, 30)

        self.nodes = self.get_nodes()
        return self.queue_network, self.nodes

    def get_nodes(self):
        return get_nodes_from_input_data(self.input_data, SimulatorConfigurations.convert_node_function)

    @staticmethod
    def convert_node_function(node_dict, node_type, node_id, node_name, service_time):
        node, queue = [None] * 2
        queue_type = node_dict.get('queue_type', 'fifo')
        if queue_type == 'fifo':
            queue = FIFOQueue()
        if node_type == 'single_server':
            node = SingleServer(node_id, node_name, service_time, queue=queue)
        elif node_type == 'infinite_server':
            node = InfiniteServer(node_id, node_name, service_time, queue=queue)
        elif node_type == 'erlang_server':
            erlang_k = node_dict['erlang_k']
            node = ErlangServer(node_id, node_name, service_time, queue=queue, erlang_k=erlang_k)
        elif node_type == 'project_cpu':
            service_time = array(node_dict['distribution_parameters'], dtype=getFloatNumpyType())
            node = ProjectCPU(node_id, node_name, service_time, queue=queue)

        return node

    def convert_output_data(self, simulator):
        interval_stats = simulator.stats.report()
        nodes_stats = list()
        test_response_time_refence_node = check_on_response_time(simulator.nodes, self.reference_node)
        for node in simulator.nodes:
            node_stats = node.stats.report(simulator.simulator_time)
            node_stats['id'] = node.id
            node_stats['name'] = node.name
            node_stats['visits'] = toFloatNumpy(node.stats.visit)
            mean, variance = node.service_stats.mean_and_variance()
            node_stats['service_time_mean'] = mean
            node_stats['service_time_variance'] = variance
            node_stats['service_time_theoretical_variance'] = node.theoretical_variance
            node_counter = list(simulator.system.counter[node.id])
            total_counter = sum(node_counter) * 1.0
            counters = []
            if total_counter:
                counters = [counter / total_counter for counter in node_counter]
            node_stats['transaction_counters'] = node_counter
            node_stats['transaction_probability'] = counters
            nodes_stats.append(node_stats)
        output_data = dict()
        output_data['integral_response_time_value'] = test_response_time_refence_node
        output_data['integral_area_value'] = sum(
            [n.stats.area for n in simulator.nodes if n.id != self.reference_node])
        output_data['integral_area_counter'] = simulator.nodes[self.reference_node].stats.N_departures
        confidence_interval = simulator.stats.confidence_interval
        output_data['confidence_interval_area_value'] = confidence_interval.a_sum
        output_data['confidence_interval_area_counter'] = confidence_interval.v_sum
        output_data['confidence_interval_mean_counter'] = confidence_interval.v_sum / confidence_interval.total
        output_data[NODES] = nodes_stats
        output_data[CONFIDENCE_INTERVAL] = interval_stats
        output_data[NUMBER_OF_CUSTOMERS] = self.number_of_customers
        output_data[REFERENCE_NODE] = self.reference_node
        output_data['events'] = simulator.number_events
        output_data['simulation_time'] = simulator.simulator_time
        output_data['number_of_client_on_reference_node'] = simulator.nodes[self.reference_node].number_clients
        return output_data

    def convert_output_data_for_csv(self, output_data):
        """

        :type output_data: dict
        """
        rows = list()

        def conver_node(run_number, node_dict, node_header):
            nodes = node_dict.get(NODES, [])
            rows = []
            for node in nodes:
                rows.append([run_number] + [node[key] for key in node_header])
            return rows

        runs_list = output_data.get('runs', [])
        if len(runs_list):
            node_header = ['id', 'name', 'area', 'busy_time', 'mean_number_of_customers',
                           'average_W', 'departures', 'throughput',
                           'load', 'max_number_of_client']
            rows.append(['run_number'] + node_header)

            for run in runs_list:
                rows += conver_node(run.get('run'), run, node_header)

            run_header = ['run', 'integral_area_value', 'integral_area_counter', 'integral_area_mean_test',
                          'confidence_interval_area_value', 'confidence_interval_area_counter',
                          'confidence_interval_mean_test', 'events',
                          'number_of_client_on_reference_node', 'simulation_time',
                          'integral_response_time_value']
            interval_header = [MEAN_INTERVAL, MAX_INTERVAL, MIN_INTERVAL, 'absolute_error', PRECISION_INTERVAL]

            rows.append(run_header + interval_header)
            for run in runs_list:
                run_list = [run[key] for key in run_header if key in run]
                interval_list = [run['confidence_interval'][key] for key in interval_header]
                rows.append(run_list + interval_list)

        if 'tests_intervals' in output_data:
            test_intervals = output_data['tests_intervals']['confidence_interval']
            test_interval_header = ['runs', 'in', 'out', 'in_bottom', 'in_top',
                                    'out_bottom', 'out_top', POINT_OF_REGENERATION_COUNTER]
            rows.append(test_interval_header)
            rows.append(
                [len(runs_list)] + [test_intervals[key] for key in test_interval_header if key in test_intervals])

        return rows