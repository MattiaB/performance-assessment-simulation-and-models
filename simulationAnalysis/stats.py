__author__ = 'Mattia Bertorelllo'

from math import sqrt
from scipy import stats
import configurations


class MeanVariance(object):
    """
    Keeps and calculates the mean and variance data
    """

    def __init__(self):
        self.sum = 0.0
        self.sum_square = 0.0
        self.count = 0.0

    def add_value(self, value):
        self.sum += value
        self.sum_square += value ** 2
        self.count += 1

    def mean_and_variance(self):
        """
        Calculates the mean and variance

        :rtype : float, float
        :return: mean, variance
        """
        mean = self.sum / self.count
        mean_square = self.sum_square / self.count
        variance = mean_square - mean ** 2
        return mean, variance


class ConfidenceInterval(object):
    """
    Keeps and calculates the confidence interval data
    """

    def __init__(self, confidence=0.95):
        self.a_sum, self.saa, self.sav, self.svv, self.v_sum, self.total = [0.0] * 6
        self.confidence = confidence

    def add_value(self, value, weights):
        self.a_sum += value
        self.v_sum += weights
        self.saa += value ** 2
        self.sav += value * weights
        self.svv += weights ** 2
        self.total += 1

    def get(self):
        p, saa, sav, svv, sv = self.total, self.saa, self.sav, self.svv, self.v_sum
        mean = self.a_sum / self.v_sum
        valuer = sqrt(p / (p - 1)) * (sqrt(saa - (2. * mean * sav) + (mean ** 2. * svv)) / sv)
        distribution = stats.t._ppf((1 + self.confidence) / 2., p - 1)
        absolute_error = distribution * valuer
        sx = mean - absolute_error
        dx = mean + absolute_error
        precision = abs(absolute_error) / mean
        return mean, sx, dx, absolute_error, precision


class StatsAccumulator:
    """
    Stats data keeper for each node
    also generates a dict report
    """

    def __init__(self):
        self.N_system = 0.
        self.N_max = 1.
        self.N_departures = 0.
        self.busy_time = 0.0
        self.area = 0.0

    def new_event(self, interval):
        N_system = self.N_system
        N_max = self.N_max
        if N_system > 0:
            self.busy_time += interval
            self.area += interval * N_system

        if N_system > N_max:
            self.N_max = N_system

    def arrival(self):
        self.N_system += 1

    def departure(self):
        self.N_system -= 1
        self.N_departures += 1

    def report(self, simulator_time):
        load = self.busy_time / simulator_time
        throughput = self.N_departures / simulator_time
        average_W, service_time = 0, 0
        if self.N_departures is not 0:
            average_W = self.area / self.N_departures
            service_time = self.busy_time / self.N_departures

        average_N = self.area / simulator_time

        output_data = dict()
        output_data['area'] = self.area
        output_data['busy_time'] = self.busy_time
        output_data['service_time'] = service_time
        output_data['mean_number_of_customers'] = average_N
        output_data['average_W'] = average_W
        output_data['departures'] = self.N_departures
        output_data['throughput'] = throughput
        output_data['load'] = load
        output_data['max_number_of_client'] = self.N_max
        return output_data


class StatsSimulator:
    """
    Stats data keeper for the simulator
    also generates a dict report
    """

    def __init__(self, confidence=0.95):
        self.confidence_interval = ConfidenceInterval(confidence)
        self.response_time_sum = 0.
        self.point_of_regeneration_counter = 0.
        self.confidence = confidence

    def add_values_for_confidence_interval(self, area, counter):
        self.confidence_interval.add_value(area, counter)
        self.point_of_regeneration_counter += 1

    def interval_precision(self):
        precision = None
        # Very important
        if self.confidence_interval.total > 20:
            _, _, _, _, precision = self.confidence_interval.get()
        return precision

    def report(self):
        if self.confidence_interval.total > 2:
            response_time_mean, sx, dx, absolute_error, precision = self.confidence_interval.get()
            output_data = dict()
            output_data[configurations.PRECISION_INTERVAL] = precision
            output_data['absolute_error'] = absolute_error
            output_data[configurations.MEAN_INTERVAL] = response_time_mean
            output_data[configurations.MAX_INTERVAL] = dx
            output_data[configurations.MIN_INTERVAL] = sx
            output_data[configurations.POINT_OF_REGENERATION_COUNTER] = self.confidence_interval.total
            return output_data
        else:
            return None


def check_on_response_time(nodes, reference_node_id):
    reference_node = nodes[reference_node_id]
    departure_reference_node = reference_node.stats.N_departures * 1.0
    for node in nodes:
        node.stats.visit = node.stats.N_departures / departure_reference_node
    nodes = [n for n in nodes if n.stats.N_departures]
    cycle_time = sum(map(lambda n: n.stats.visit * (n.stats.area / n.stats.N_departures), nodes))
    return cycle_time - (reference_node.stats.area / reference_node.stats.N_departures)


class AreaAccumulator(object):
    def __init__(self, area_id, area=0., counter=0.):
        self.id = area_id
        self.counter = area
        self.area = counter
        self.before_passage_time = 0.

    def add_time(self, simulator_time):
        if simulator_time != self.before_passage_time:
            self.area += simulator_time - self.before_passage_time
            self.counter += 1

    def set_before_time(self, simulator_time):
        self.before_passage_time = simulator_time

    def reset_only_area(self):
        self.counter = 0.
        self.area = 0.
        return self


class TestIntervals(object):
    """
    Calculate the dict for confidence interval analysis
    """

    def __init__(self, theoretical_value, limited=False):
        if not limited:
            self.test_intervals = dict.fromkeys(['in', 'out', 'in_bottom', 'in_top', 'out_bottom', 'out_top'], 0)
        else:
            self.test_intervals = dict.fromkeys(['bottom', 'top'], 0)
        self.theoretical_value = theoretical_value

    def put_confidence_interval(self, confidence_interval):
        mean_interval = confidence_interval[configurations.MEAN_INTERVAL]
        max_interval = confidence_interval[configurations.MAX_INTERVAL]
        min_interval = confidence_interval[configurations.MIN_INTERVAL]
        if max_interval >= self.theoretical_value >= min_interval:
            where_it_is = 'in'
            if mean_interval < self.theoretical_value:
                where_it_is_nested = 'in_bottom'
            else:
                where_it_is_nested = 'in_top'
        else:
            where_it_is = 'out'
            if max_interval < self.theoretical_value:
                where_it_is_nested = 'out_bottom'
            else:
                where_it_is_nested = 'out_top'
        self.test_intervals[where_it_is] += 1
        self.test_intervals[where_it_is_nested] += 1
        return where_it_is, where_it_is_nested

    def put_mean(self, mean):
        if mean < self.theoretical_value:
            where_it_is = 'bottom'
        else:
            where_it_is = 'top'
        self.test_intervals[where_it_is] += 1
        return where_it_is