# coding=utf-8

from simulationAnalysis.configurations import SimulatorConfigurations, MEAN_INTERVAL
from simulationAnalysis.log import Log
from simulationAnalysis.models import *
from simulationAnalysis.stats import StatsSimulator, TestIntervals


__author__ = 'Mattia Bertorello'


class Simulator:
    def __init__(self, system, number_of_clients, reference_node, precision, steps_number=30, event_to_print=-1):
        self.precision = precision
        self.number_of_clients = number_of_clients
        self.steps_number = steps_number
        self.reference_node_id = reference_node
        self.system = system
        self.nodes = system.nodes
        self.number_events = 0.
        self.simulator_time = 0.
        self.go_on = True
        self.log = Log(self, event_to_print)
        self.prepare_simulator()

    def prepare_simulator(self):
        """
        Prepare the simulator to run
        new stats, event list,

        """
        self.number_events = 0.
        self.simulator_time = 0.
        self.go_on = True
        self.event_list = EventList()

        reference_node = self.system.nodes[self.reference_node_id]
        self.clients = []
        for i in range(0, self.number_of_clients):
            self.clients.append(Client(i, reference_node))

        self.stats = StatsSimulator()

        def init_event(client, node, simulator_time):
            new_event = Event()
            new_event.client = client
            return node.set_incoming(new_event, simulator_time)

        reference_node = self.nodes[self.reference_node_id]

        for i in range(0, self.number_of_clients):
            event = init_event(self.clients[i], reference_node, self.simulator_time)
            self.event_list.put(event)

    def engine(self):
        """
        Start the simulator engine
        @return dict simulator stats report
        """
        while self.go_on:

            event = self.event_list.get()
            old_time = self.simulator_time
            self.simulator_time = event.occurrence_time
            interval = self.simulator_time - old_time
            for node in self.nodes:
                node.stats.new_event(interval)

            self.log.print_stats_before_transaction()

            if event.type == Event.departure:
                self.make_transaction(event)
            elif event.type == Event.end:
                self.go_on = False

            self.number_events += 1

        self.log.print_at_the_end_of_simulator()
        return self.stats.report()

    def make_transaction(self, departure_event):
        """
        Execute a complete transaction from a node to another
        Before set a departure from departure_event.node
        and make a point of regeneration
        then set a arrival into arrival node definied by self.system.next_node(departure_event.node)
        @param Event departure_event:
        """
        possible_new_event = departure_event.node.set_outcoming(departure_event, self.simulator_time)
        self.event_list.put(possible_new_event)

        self.log.print_after_exit_of_node(departure_event)

        self.point_of_regeneration(departure_event)

        next_node = self.system.next_node(departure_event.node)
        new_departure_event = next_node.set_incoming(departure_event, self.simulator_time)

        self.log.print_after_arrival_of_node(next_node)

        self.event_list.put(new_departure_event)

    def point_of_regeneration(self, event):
        """
        Check if this event is a point of regeneration:
        - if the first node is empty
        - if all client area counter are more than a minimum number of times
        sum all areas and counter of clients and add these values at confidence interval
        check if the precision is lower than the config precision
        if so stop the simulation
        @param Event event:
        @return :
        """
        if event.node.id == 1 and event.type == Event.departure and not self.nodes[1].number_clients:
            steps_number = self.steps_number
            result = all([client.area.counter > steps_number for client in self.clients])
            if result:
                total_area, total_counter = [0.0] * 2
                for client in self.clients:
                    total_area += client.area.area
                    total_counter += client.area.counter
                    client.reset_only_area()

                if self.stats.point_of_regeneration_counter:
                    self.stats.add_values_for_confidence_interval(total_area, total_counter)
                else:
                    self.stats.point_of_regeneration_counter += 1
                precision = self.stats.interval_precision()
                if precision and precision < self.precision:
                    self.go_on = False

        return self.go_on


def start_simulator(input_data, do_csv):
    config = SimulatorConfigurations()
    queue_network, _ = config.convert_input_data(input_data)
    number_of_clients = config.number_of_customers
    total_node_output_data = list()
    if config.simulator_seed:
        random.seed(config.simulator_seed)
    main_test_intervals = TestIntervals(config.theoretical_value)
    integral_area_test_intervals = TestIntervals(config.theoretical_value, limited=True)
    confidence_interval_test_intervals = TestIntervals(config.theoretical_value, limited=True)

    for run_number in xrange(config.simulation_runs):
        nodes = config.get_nodes()
        system = System(queue_network, nodes)
        simulator = Simulator(system, number_of_clients, config.reference_node, config.precision,
                              steps_number=config.steps_number)
        interval_stats = simulator.engine()
        sum_area_nodes = sum([n.stats.area for n in simulator.nodes if n.id != simulator.reference_node_id])
        integral_area_mean = sum_area_nodes / simulator.nodes[config.reference_node].stats.N_departures

        main_test_intervals.put_confidence_interval(interval_stats)

        integral_test_top_or_bottom = integral_area_test_intervals.put_mean(integral_area_mean)
        confidence_test_top_or_bottom = confidence_interval_test_intervals.put_mean(interval_stats[MEAN_INTERVAL])

        if config.keep_intervals or config.simulation_runs == 1:
            output_data = config.convert_output_data(simulator)
            output_data['integral_area_mean_test'] = integral_test_top_or_bottom
            output_data['confidence_interval_mean_test'] = confidence_test_top_or_bottom
            output_data['run'] = run_number
            total_node_output_data.append(output_data)

        different_run_number = config.simulation_runs - run_number
        test_intervals = main_test_intervals.test_intervals
        print "End of {0} simulation, {1} simulations remain".format(run_number, different_run_number), test_intervals

    final_output_data = dict()
    if config.test_simulator:
        tests_intervals = dict()
        tests_intervals['confidence_interval'] = main_test_intervals.test_intervals
        tests_intervals['integral_area_mean'] = integral_area_test_intervals.test_intervals
        tests_intervals['confidence_interval_mean'] = confidence_interval_test_intervals.test_intervals
        final_output_data['tests_intervals'] = tests_intervals
    final_output_data['runs'] = total_node_output_data
    final_output_data['input_data'] = config.input_data
    output_data_csv = []
    if do_csv:
        output_data_csv = config.convert_output_data_for_csv(final_output_data)
    return final_output_data, output_data_csv


if __name__ == '__main__':
    import sys

    file_name = sys.argv[1]
    output_filename = sys.argv[2]
    do_csv = False
    if len(sys.argv) >= 4 and sys.argv[3] == 'csv':
        do_csv = True
    input_data = get_input_data_from_file_json(file_name)
    output_data, output_data_csv = start_simulator(input_data, do_csv)
    write_output_data(output_data, output_filename)
    if do_csv:
        write_output_data_csv(output_data_csv, output_filename)
        # import gc
        # import pstats
        # import cProfile
        # #gc.set_debug(gc.DEBUG_STATS)
        # cProfile.run('start_simulator(input_data, True)', 'restats')
        # #gc.collect()
        # p = pstats.Stats('restats')
        # p.sort_stats("time").print_stats(20)
        # p.sort_stats("cumulative").print_stats(20)