from simulationAnalysis.models import Client

__author__ = 'Mattia Bertorello'


class Log:
    def __init__(self, simulator, event_to_print=-1):
        self.event_to_print = event_to_print
        self.simulator = simulator

    def print_event(self, event, old_time):
        simulator = self.simulator
        if simulator.number_events < self.event_to_print:
            print 'event numeber: {}'.format(simulator.number_events)
            print 'simulation time: {}'.format(simulator.simulator_time)
            print 'simulation old_time: {}'.format(old_time)
            print 'occurrence_time: {}'.format(event.occurrence_time)
            print 'node: {}'.format(event.node.id)
            print 'node N_system: {}'.format(event.node.stats.N_system)
            print 'node N_departures: {}'.format(event.node.stats.N_departures)
            print 'node busy_time: {}'.format(event.node.stats.busy_time)
            print 'node area: {}'.format(event.node.stats.area)
            print 'client: {}'.format(event.client.name)
            print 'client start time: {}'.format(event.client.before_passage_time)
            steps_number = event.client.steps_number
            if not steps_number:
                steps_number = 1
            print 'client response time: {}'.format(event.client.response_time / steps_number)
            print

    def print_stats_before_transaction(self):
        simulator = self.simulator
        if simulator.number_events < self.event_to_print:
            print "--------Dati prima di eseguire la transizione--------"
            print "simulation_time: {} before: {}".format(simulator.simulator_time,
                                                          simulator.nodes[simulator.reference_node_id].b_2)
            print simulator.event_list._data
            total_client_area, total_client_counter = Client.area_and_counter_clients_total(simulator.clients)
            print "Total client area: {}, counter: {}".format(
                total_client_area + simulator.stats.confidence_interval.a_sum,
                simulator.stats.confidence_interval.v_sum + total_client_counter)
            print "Total area per ogni nodo tranne i terminali: {}".format(
                sum([n.stats.area for n in simulator.nodes if n.id != simulator.reference_node_id]))
            print '--------------------------------------------------------'

    def print_at_the_end_of_simulator(self):
        simulator = self.simulator
        if simulator.number_events < self.event_to_print:
            print "Total area per ogni nodo tranne i terminali: {}, partenze: {}".format(
                sum([n.stats.area for n in simulator.nodes if n.id != simulator.reference_node_id]),
                simulator.nodes[simulator.reference_node_id].stats.N_departures)
            print "Area intervallo di confidenza {}, counter {}".format(simulator.stats.confidence_interval.a_sum,
                                                                        simulator.stats.confidence_interval.v_sum)
            total_client_area, total_client_counter = Client.area_and_counter_clients_total(simulator.clients)
            print "Total client area: {}, counter: {}".format(
                total_client_area + simulator.stats.confidence_interval.a_sum,
                simulator.stats.confidence_interval.v_sum + total_client_counter)
            print "{} clienti {}".format(simulator.nodes[simulator.reference_node_id].name,
                                         simulator.nodes[simulator.reference_node_id].number_clients)

    def print_after_exit_of_node(self, event):
        simulator = self.simulator
        if simulator.number_events < self.event_to_print:
            print "--------Dati dopo l'uscita dalla stazione {}--------".format(event.node.name)
            print "Total area per ogni nodo tranne i terminali: {}".format(
                sum([n.stats.area for n in simulator.nodes if n.id != simulator.reference_node_id]))
            print simulator.event_list._data
            print '--------------------------------------------------------'

    def print_after_arrival_of_node(self, next_node):
        simulator = self.simulator
        if simulator.number_events < self.event_to_print:
            print "--------Dati dopo l'arrivo dalla stazione {} --------".format(next_node.name)
            print "Total area per ogni nodo tranne i terminali: {}".format(
                sum([n.stats.area for n in simulator.nodes if n.id != simulator.reference_node_id]))
            print simulator.event_list._data
            total_area, total_counter = [0] * 2
            for client in simulator.clients:
                total_area += client.area.area
                total_counter += client.area.counter
            print "Area totale dei clienti: {}, counter: {}".format(total_area, total_counter)
            print '--------------------------------------------------------'
            print
            print