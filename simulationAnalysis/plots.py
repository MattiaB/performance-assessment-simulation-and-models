__author__ = 'Mattia Bertorello'

import matplotlib.pyplot as plt
#import numpy as np


class ModelPlots:
    def __init__(self, plot_file_path):
        self.plotFilePath = plot_file_path

    def response_time_plot(self, numpy_range, max_response_time_func, one_client_response_time_func):
        fig = plt.figure()
        plt.title('Response Time')
        plt.xlabel('Number of Client')
        plt.ylabel('Time')

        p1 = plt.plot(numpy_range, max_response_time_func(numpy_range), label="NVbSb - Z")
        p2 = plt.plot(numpy_range, one_client_response_time_func(numpy_range), label="E ViSi")

        fig.savefig(self.plotFilePath + 'response_time.png')

