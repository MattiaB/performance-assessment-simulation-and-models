# coding=utf-8
import collections
import heapq

from numpy import cumsum, random, zeros

from helpers.convert_data import *
from simulationAnalysis.stats import MeanVariance, AreaAccumulator, StatsAccumulator


class Client(object):
    """
        Keep the area time into an `AreaAccumulator` object
    """

    def __init__(self, client_id, reference_node):
        self.id = client_id
        self.reference_node = reference_node
        self.area = AreaAccumulator(client_id)

    def arrival(self, node, simulator_time):
        if node.id == self.reference_node.id:
            self.area.add_time(simulator_time)

    def departure(self, node, simulator_time):
        if node.id == self.reference_node.id:
            self.area.set_before_time(simulator_time)

    def reset_only_area(self):
        self.area.reset_only_area()

    @classmethod
    def area_and_counter_clients_total(cls, clients):
        total_area, total_counter = [0.0] * 2
        for client in clients:
            total_area += client.area.area
            total_counter += client.area.counter
        return total_area, total_counter


class Event:
    """
    Is a Event into simulator
    `self.node` is the node where the event is in that time
    `self.client` is the client associate at the beginning of simulation
    """
    departure = 0  # Departure type
    end = 2        # End type

    def __init__(self, event_type=None, occurrence_time=None, node=None, client=None):
        self.type = event_type
        self.occurrence_time = occurrence_time
        self.node = node
        self.client = client

    @staticmethod
    def get_end(occurrence_time):
        return Event(Event.end, occurrence_time)

    def __repr__(self):
        return "node: {}, occurrence_time: {}, client:  {}".format(self.node.name, self.occurrence_time,
                                                                   self.client.id)


class EventList:
    """
    Structure data where the events are kept in order of occurrence time
    """

    def __init__(self):
        self._data = []

    def put(self, event):
        if event:
            key = event.occurrence_time
            heapq.heappush(self._data, (key, event))
        return self

    def get(self):
        return heapq.heappop(self._data)[1]


class FIFOQueue:
    def __init__(self):
        self._queue = collections.deque()
        self.length = 0

    def get(self):
        self.length -= 1
        return self._queue.popleft()

    def put(self, elem):
        self.length += 1
        return self._queue.append(elem)

    def __len__(self):
        return self.length


class Node(object):
    """
    General type of node with exponential distribution of service time
    """

    def __init__(self, node_id, name, service_time_value=None, queue=None):
        self.id = node_id
        self.name = name
        self.service_time_value = service_time_value
        self.queue = queue
        self.number_clients = 0.
        self.load_dependent = False
        self.service_stats = MeanVariance()
        self.theoretical_variance = 0
        self.stats = StatsAccumulator()

    def service_time(self, queue_len=1):
        service_time = random.exponential(self.service_time_value)
        self.service_stats.add_value(service_time)
        return service_time

    def base_incoming(self, event, simulator_time):
        self.number_clients += 1
        event.node = self
        # Statistic Update
        self.stats.arrival()
        event.client.arrival(self, simulator_time)
        return event

    def set_incoming(self, event, simulator_time):
        event = self.base_incoming(event, simulator_time)

        # Genera il tempo di servizio del cliente appena arrivato
        delta_service = self.service_time()
        event.created_time = simulator_time
        event.service_time = delta_service

        if self.number_clients == 1:
            # Genera evento partenza nel caso di sistema vuoto
            leaving_time = simulator_time + delta_service
            event.type = Event.departure
            event.occurrence_time = leaving_time
        else:
            # Aggiungi event-motice in coda al servitore occupato
            self.queue.put(event)
            event = None

        return event

    def base_outcoming(self, event, simulator_time):
        self.number_clients -= 1
        # Statistic Update
        self.stats.departure()
        event.client.departure(self, simulator_time)
        return event

    def set_outcoming(self, event, simulator_time):
        self.base_outcoming(event, simulator_time)
        new_leaving = None

        if self.queue.length > 0:
            # Preleva event-notice dalla coda di ingresso del serivtore
            new_leaving = self.queue.get()
            delta_service = new_leaving.service_time
            # Genera evento partenza perchè il è sistema vuoto
            leaving_time = simulator_time + delta_service
            new_leaving.created_time = simulator_time

            new_leaving.occurrence_time = leaving_time

        return new_leaving


class SingleServer(Node):
    def __init__(self, node_id, name, service_time_value=None, queue=None):
        super(SingleServer, self).__init__(node_id, name, service_time_value, queue)
        self.theoretical_variance = 1. / (1. / service_time_value) ** 2


class InfiniteServer(Node):
    def __init__(self, node_id, name, service_time_value=None, queue=None):
        super(InfiniteServer, self).__init__(node_id, name, service_time_value, queue)
        self.theoretical_variance = 1. / (1. / service_time_value) ** 2
        self.load_dependent = True

    def set_incoming(self, event, simulator_time):
        event = self.base_incoming(event, simulator_time)

        # Genera il tempo di servizio del cliente appena arrivato
        delta_service = self.service_time()
        leaving_time = simulator_time + delta_service

        event.type = Event.departure
        event.created_time = simulator_time
        event.service_time = delta_service
        event.occurrence_time = leaving_time

        return event


class ErlangServer(Node):
    def __init__(self, node_id, name, service_time_value=None, queue=None, erlang_k=1):
        super(ErlangServer, self).__init__(node_id, name, service_time_value, queue)
        self.erlang_k = erlang_k
        self.theoretical_variance = self.erlang_k / (1. / service_time_value) ** 2

    def service_time(self, queue_len=1):
        service_time = 1. / random.gamma(self.erlang_k, 1. / self.service_time_value)
        self.service_stats.add_value(service_time)
        return service_time


class ProjectCPU(Node):
    def service_time(self, queue_len=1):
        r1 = random.uniform(self.service_time_value[0], self.service_time_value[1])
        r2 = random.uniform(self.service_time_value[2], self.service_time_value[3])
        r3 = -1
        while r3 < 0 or r3 > 30:
            r3 = random.normal(self.service_time_value[4], self.service_time_value[5])
        service_time = (r1 + r2 + r3) / 1000.
        self.service_stats.add_value(service_time)
        return service_time


class System:
    """
    Keep the queue network and the nodes
    moreover calculate the next node based on origin node
    """

    def __init__(self, queue_network, nodes):
        for node in nodes:
            node.system = self
        self.queue_network = queue_network
        node_number = len(self.queue_network)
        aux_cumulative_queue_network = []
        for i in range(node_number):
            cumulative_queue_network = cumsum(queue_network[i])
            cumulative_queue_network = toFloatNumpy(fromNumpyTypeToString(cumulative_queue_network))
            aux_cumulative_queue_network.append(list(enumerate(cumulative_queue_network)))
        self.cumulative_queue_network = aux_cumulative_queue_network
        self.counter = zeros((node_number, node_number), dtype=getFloatNumpyType())
        self.nodes = nodes

    def next_node(self, from_node):
        cumulative_queue_network_node = self.cumulative_queue_network[from_node.id]
        random_number = random.uniform()
        index_next_node = 0
        for index_next_node, v in cumulative_queue_network_node:
            if v >= random_number:
                break
        self.counter[from_node.id][index_next_node] += 1
        return self.nodes[index_next_node]