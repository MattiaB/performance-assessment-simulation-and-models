from helpers.convert_data import *
from operationAnalysis.models import SingleServer, InfiniteServer

__author__ = 'Mattia Bertorello'

BOTTLENECK = 'BOTTLENECK'
HIDDEN_BOTTLENECK = 'HIDDEN_BOTTLENECK'
THROUGHPUT_BOTTLENECK = 'THROUGHPUT_BOTTLENECK'
THROUGHPUT_HIDDEN_BOTTLENECK = 'THROUGHPUT_HIDDEN_BOTTLENECK'
RESPONSE_TIME_BOTTLENECK = 'RESPONSE_TIME_BOTTLENECK'
RESPONSE_TIME_HIDDEN_BOTTLENECK = 'RESPONSE_TIME_HIDDEN_BOTTLENECK'
BOTTLENECK_DATA = 'BOTTLENECK_DATA'
VISIT_VECTOR = 'VISIT_VECTOR'
SATURATION_POINT = 'SATURATION_POINT'
THROUGHPUT_REFERENCE_NODE = 'THROUGHPUT_REFERENCE_NODE'
RESPONSE_TIME_1 = 'RESPONSE_TIME_1'
REFERENCE_NODE = 'reference_node'


class OperationalAnalysisConfigurations(GeneralConfigurations):
    def convert_input_data(self, input_data):
        self.queue_network = get_queue_network_from_input_data(input_data)
        standard_node_function = get_convert_standard_node_function(SingleServer, InfiniteServer)
        self.nodes = get_nodes_from_input_data(input_data, standard_node_function)
        self.reference_node = input_data.get(REFERENCE_NODE, 0)
        self.number_of_customers = input_data.get(NUMBER_OF_CUSTOMERS, 1)

    def convert_output_data(self, output):
        output_bottleneck = dict()
        output_bottleneck[BOTTLENECK] = convert_node(output[BOTTLENECK_DATA][BOTTLENECK])
        output_bottleneck[HIDDEN_BOTTLENECK] = convert_node(output[BOTTLENECK_DATA][HIDDEN_BOTTLENECK])
        output_bottleneck[THROUGHPUT_BOTTLENECK] = fromNumpyTypeToString(output[BOTTLENECK_DATA][THROUGHPUT_BOTTLENECK])
        output_bottleneck[THROUGHPUT_HIDDEN_BOTTLENECK] = fromNumpyTypeToString(
            output[BOTTLENECK_DATA][THROUGHPUT_HIDDEN_BOTTLENECK])
        output_bottleneck[RESPONSE_TIME_BOTTLENECK] = fromNumpyTypeToString(
            output[BOTTLENECK_DATA][RESPONSE_TIME_BOTTLENECK])
        output_bottleneck[RESPONSE_TIME_HIDDEN_BOTTLENECK] = fromNumpyTypeToString(
            output[BOTTLENECK_DATA][RESPONSE_TIME_HIDDEN_BOTTLENECK])

        convert_output = convert_output_data(output, self.number_of_customers)
        convert_output[BOTTLENECK_DATA] = output_bottleneck
        convert_output[RESPONSE_TIME_1] = fromNumpyTypeToString(output[RESPONSE_TIME_1])
        convert_output[SATURATION_POINT] = fromNumpyTypeToString(output[SATURATION_POINT])
        convert_output[THROUGHPUT_REFERENCE_NODE] = fromNumpyTypeToString(output[THROUGHPUT_REFERENCE_NODE])
        convert_output[VISIT_VECTOR] = list(fromNumpyTypeToString(output[VISIT_VECTOR]))
        convert_output[REFERENCE_NODE] = self.reference_node
        return convert_output

    def convert_output_data_for_csv(self, output):
        nodes = output[NODES]
        columns = list()
        header = list()
        columns.append(range(1, output[NUMBER_OF_CUSTOMERS] + 1))
        header.append('N')
        for node in nodes:
            columns.append(node['throughput_N'])
            header.append('throughput_N_' + str(node['id']))
        columns.append(output[THROUGHPUT_REFERENCE_NODE])
        header.append(THROUGHPUT_REFERENCE_NODE)
        for node in nodes:
            columns.append(node['response_time_N'])
            header.append('response_time_N_' + str(node['id']))

        for node in nodes:
            columns.append(node['load_N'])
            header.append('load_N' + str(node['id']))

        columns.append([output[BOTTLENECK_DATA][THROUGHPUT_BOTTLENECK]] * output[NUMBER_OF_CUSTOMERS])
        columns.append([output[BOTTLENECK_DATA][THROUGHPUT_HIDDEN_BOTTLENECK]] * output[NUMBER_OF_CUSTOMERS])
        columns.append(output[BOTTLENECK_DATA][RESPONSE_TIME_BOTTLENECK])
        columns.append(output[BOTTLENECK_DATA][RESPONSE_TIME_HIDDEN_BOTTLENECK])
        columns.append([output[RESPONSE_TIME_1]] * output[NUMBER_OF_CUSTOMERS])
        for title in [THROUGHPUT_BOTTLENECK, THROUGHPUT_HIDDEN_BOTTLENECK,
                      RESPONSE_TIME_BOTTLENECK, RESPONSE_TIME_HIDDEN_BOTTLENECK, RESPONSE_TIME_1]:
            header.append(title)
        csv_output_data = zip(*columns)
        csv_output_data = [header] + csv_output_data
        return csv_output_data
