# coding=utf-8
from helpers.convert_data import getFloatNumpyType, toFloatNumpy

from models import *
from operationAnalysis.configurations import *

__author__ = 'Mattia Bertorello'


def convolution_algorithm(nodes, N):
    """
    `Buzen's algorithm <http://en.wikipedia.org/wiki/Buzen%27s_algorithm>` (or convolution algorithm)
    Calculates the normalization constant G(N)

    :type nodes: list
    :type N: int
    :rtype : numpy.ndarray
    :param nodes: 
    :param N: 
    """
    N += 1
    #nodes = [None] + nodes
    M = len(nodes)  # Numero di stazioni del sistema
    # Inizializzazione delle Costante di Normalizzazione
    G = np.zeros(N, dtype=getFloatNumpyType())
    f = np.zeros(N, dtype=getFloatNumpyType())
    G[0] = toFloatNumpy(1.0)
    # Calcolo della Costante di Normalizzazione
    for i in range(0, M):
        if nodes[i].__class__ != InfiniteServer:
            y = nodes[i].visit_ratio * nodes[i].service_time()
            for k in range(1, N):
                G[k] += y * G[k - 1]
        else:
            f[0] = toFloatNumpy(1.0)
            for k in range(1, N):
                f[k] = f[k - 1] * nodes[i].visit_ratio * nodes[i].service_time(k)
            for n in reversed(range(1, N)):
                total = G[n]
                for k in range(1, n + 1):
                    total += f[k] * G[n - k]
                G[n] = total

    return G


def visit_array(queue_network, reference_station):
    """
    Calculates the visits of each node

    :type queue_network: numpy.ndarray
    :type reference_station: int
    :rtype : numpy.ndarray
    :param queue_network:
    :param reference_station:
    """
    eq_to_remove = reference_station
    delete_column = 1
    delete_row = reference_station
    matrix_dim = queue_network.shape[0]
    I = np.identity(matrix_dim)
    queue_network = queue_network - I
    coefficients = queue_network[eq_to_remove] * -1

    queue_network_transpose = queue_network.transpose()

    queue_network_transpose = np.delete(queue_network_transpose, eq_to_remove, delete_column)
    queue_network_transpose = np.delete(queue_network_transpose, eq_to_remove, delete_row)

    coefficients = np.delete(coefficients, eq_to_remove, delete_row)

    visit_array = np.linalg.solve(queue_network_transpose, coefficients)
    visit_array = np.insert(visit_array, eq_to_remove, toFloatNumpy(1))
    visit_array = np.float64(visit_array.astype('|S20'))

    return visit_array


def bottleneck_analysis(nodes, number_of_customers, reference_station):
    independent_load_nodes = [node for node in nodes if not node.load_dependent]
    key_func = lambda station: station.get_service_time_multiplied_requests()
    #Calcolo della bottleneck
    bottleneck = max(independent_load_nodes, key=key_func)
    nodes_without_bottleneck = [node for node in independent_load_nodes if node.id != bottleneck.id]
    #Calcolo della bottleneck nascosta
    hidden_bottleneck = max(nodes_without_bottleneck, key=key_func)

    bottleneck_s_per_r = bottleneck.get_service_time_multiplied_requests()
    hidden_bottleneck_s_per_r = hidden_bottleneck.get_service_time_multiplied_requests()

    response_time_bottleneck_N = [r_n * bottleneck_s_per_r - reference_station.service_time_value
                                  for r_n in range(1, number_of_customers + 1)]
    response_time_hidden_bottleneck_N = [r_n * hidden_bottleneck_s_per_r - reference_station.service_time_value
                                         for r_n in range(1, number_of_customers + 1)]

    throughput_bottleneck = 1 / bottleneck_s_per_r
    throughput_hidden_bottleneck = 1 / hidden_bottleneck_s_per_r

    output_data_bottleneck = dict()
    output_data_bottleneck[BOTTLENECK] = bottleneck
    output_data_bottleneck[HIDDEN_BOTTLENECK] = hidden_bottleneck
    output_data_bottleneck[THROUGHPUT_BOTTLENECK] = throughput_bottleneck
    output_data_bottleneck[THROUGHPUT_HIDDEN_BOTTLENECK] = throughput_hidden_bottleneck
    output_data_bottleneck[RESPONSE_TIME_BOTTLENECK] = response_time_bottleneck_N
    output_data_bottleneck[RESPONSE_TIME_HIDDEN_BOTTLENECK] = response_time_hidden_bottleneck_N

    return output_data_bottleneck


def set_node_with_throughput_mean_number_of_customers_response_time_N(nodes, number_of_customers):
    """
    Calculates the nodes basic properties from the normalization constant G(N):
     Throughput, Response time, load for each node

    :type nodes: list
    :type number_of_customers: int
    :rtype : list
    :param nodes:
    :param number_of_customers:
    :return: nodes
    """
    G = convolution_algorithm(nodes, number_of_customers)

    throughput_N = [G[i - 1] / G[i] for i in range(1, number_of_customers + 1)]

    for node in nodes:
        node.throughput_N = [node.visit_ratio * throughput for throughput in throughput_N]
        node.response_time_N = [(customers + 1) / node.throughput_N[customers] - node.service_time_value
                                for customers in range(number_of_customers)]
        node.load_N = [node.throughput_N[customers] * node.service_time(customers + 1)
                       for customers in range(number_of_customers)]

    return nodes


def analysis(config):
    """

    :type config: OperationalAnalysisConfigurations
    :rtype : dict
    :param config:
    :return:
    """
    queue_network = config.queue_network
    nodes = config.nodes
    reference_station = nodes[config.reference_node]
    number_of_customers = config.number_of_customers

    # Calcolo delle visite di ogni stazione che è indipendente dal carico
    visit_vector = visit_array(queue_network, reference_station.id)
    for i, requests in enumerate(visit_vector):
        nodes[i].visit_ratio = requests

    #Calcolo della bottleneck
    output_bottleneck_analysis = bottleneck_analysis(nodes, number_of_customers, reference_station)
    bottleneck = output_bottleneck_analysis[BOTTLENECK]

    bottleneck_s_per_r = bottleneck.get_service_time_multiplied_requests()

    # Cycle time of the system with one client
    cycle_time_1 = sum(map(lambda station: station.get_service_time_multiplied_requests(), nodes))

    # Load level when certainty some client get in queue
    saturation_point = cycle_time_1 / bottleneck_s_per_r

    # Client into reference node when the system is saturation
    client_into_reference_station = \
        reference_station.service_time_value / bottleneck_s_per_r

    throughput_reference_node_N = [r_n / cycle_time_1 for r_n in range(1, number_of_customers + 1)]

    # Response time of the system with one client
    response_time_1 = cycle_time_1 - reference_station.service_time_value

    nodes = set_node_with_throughput_mean_number_of_customers_response_time_N(nodes,
                                                                              number_of_customers)

    output_data = dict()
    output_data[BOTTLENECK_DATA] = output_bottleneck_analysis
    output_data[VISIT_VECTOR] = visit_vector
    output_data[SATURATION_POINT] = saturation_point
    output_data[THROUGHPUT_REFERENCE_NODE] = throughput_reference_node_N
    output_data[RESPONSE_TIME_1] = response_time_1
    output_data[NODES] = nodes

    return output_data


def operation_analysis(input_data, do_csv=False):
    config = OperationalAnalysisConfigurations()
    config.convert_input_data(input_data)
    output_data = analysis(config)
    output_data = config.convert_output_data(output_data)
    output_data_csv = []
    if do_csv:
        output_data_csv = config.convert_output_data_for_csv(output_data)
    return output_data, output_data_csv


if __name__ == '__main__':
    import sys

    file_name = sys.argv[1]
    output_filename = sys.argv[2]
    do_csv = False
    if len(sys.argv) >= 4 and sys.argv[3] == 'csv':
        do_csv = True
    input_data = get_input_data_from_file_json(file_name)
    output_data, output_data_csv = operation_analysis(input_data, do_csv)
    write_output_data(output_data, output_filename)
    if do_csv:
        write_output_data_csv(output_data_csv, output_filename)