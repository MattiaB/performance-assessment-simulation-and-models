__author__ = 'Mattia Bertorello'


class Node(object):
    def __init__(self, id, name, service_time_value=None):
        self.id = id
        self.name = name
        self.service_time_value = service_time_value
        self.visit_ratio = 0
        self.load_dependent = False

    def service_time(self, queue_len=1):
        return self.service_time_value

    def get_service_time_multiplied_requests(self):
        return self.visit_ratio * self.service_time_value


class SingleServer(Node):
    pass


class InfiniteServer(Node):
    def __init__(self, id, name, service_time_value=None):
        super(InfiniteServer, self).__init__(id, name, service_time_value)
        self.load_dependent = True

    def service_time(self, queue_len=1):
        return super(InfiniteServer, self).service_time() / queue_len