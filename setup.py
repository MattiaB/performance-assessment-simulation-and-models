from distutils.core import setup
import os

REQUIREMENTS = [i.strip() for i in open("requirements.txt").readlines()]


def read(filename):
    """
    Utility function to read the README file.
    Used for the long_description.  It's nice, because now 1) we have a top level
    README file and 2) it's easier to type in the README file than to put a raw
    string in below ...
    :type filename: string
    :param filename:
    :return:
    """
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


setup(
    name='Performance Assessment - Simulation and Models',
    version='1.0',
    packages=['helpers', 'djangoModels', 'djangoSettings', 'operationAnalysis', 'simulationAnalysis',
              'markovChianAnalysis'],
    url='',
    license='GNU',
    author='Mattia Bertorello',
    author_email='mattia.bertorello@gmail.com',
    description='',
    long_description=read('readme.md'),
    install_requires=REQUIREMENTS
)
